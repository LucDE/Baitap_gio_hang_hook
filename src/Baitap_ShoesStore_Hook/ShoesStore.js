import React from "react";
import ProductList from "./ProductList";
import Cart from "./Cart";
export default function ShoesStore() {
  return (
    <div className="container py-5">
      <Cart />
      <ProductList />
    </div>
  );
}
