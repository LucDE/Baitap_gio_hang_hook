import React from "react";
import { connect } from "react-redux";
import { addItemAction } from "./redux/actions/cartAction";

function ProductItem({ product, addItem }) {
  let { name, image } = product;
  return (
    <div className="p-2">
      <div className="flex flex-col items-center border-4 rounded-lg border-pink-600 p-3">
        <img src={image} alt={name}></img>
        <p className="font-medium">{name}</p>
        <button
          className=" bg-pink-600 hover:bg-amber-400 p-2 mt-3 rounded text-white"
          onClick={() => {
            addItem(product);
          }}
        >
          Add to cart
        </button>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    addItem: (product) => {
      dispatch(addItemAction(product));
    },
  };
};

export default connect(null, mapDispatchToProps)(ProductItem);
