import {
  ADD_ITEM,
  DECREASE_ITEM,
  INCREASE_ITEM,
} from "../constants/cartConstants";

export const addItemAction = (payload) => ({
  type: ADD_ITEM,
  payload,
});
export const increaseItemAction = (payload) => ({
  type: INCREASE_ITEM,
  payload,
});
export const decreaseItemAction = (payload) => ({
  type: DECREASE_ITEM,
  payload,
});
