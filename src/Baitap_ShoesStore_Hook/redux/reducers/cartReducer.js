import {
  ADD_ITEM,
  DECREASE_ITEM,
  INCREASE_ITEM,
} from "../constants/cartConstants";

const initialState = {
  cart: [],
};

export const cartReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_ITEM: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id === payload.id);
      if (index === -1) {
        let newProduct = { ...payload, soLuong: 1 };
        cloneCart.push(newProduct);
      } else {
        cloneCart[index].soLuong++;
      }
      state.cart = cloneCart;
      return { ...state };
    }
    case INCREASE_ITEM: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id === payload.id);
      cloneCart[index].soLuong++;
      state.cart = cloneCart;
      return { ...state };
    }
    case DECREASE_ITEM: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id === payload.id);
      cloneCart[index].soLuong--;
      if (cloneCart[index].soLuong === 0) {
        cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      return { ...state };
    }

    default:
      return state;
  }
};
