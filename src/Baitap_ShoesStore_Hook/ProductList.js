import React, { memo } from "react";
import ProductItem from "./ProductItem";
import data from "./data.json";

const ProductList = function (props) {
  const renderContent = () => {
    return data.map((product) => {
      return <ProductItem key={product.name} product={product} />;
    });
  };
  return (
    <div className=" w-3/4 grid grid-cols-4 gap-4" style={{ margin: "0 auto" }}>
      {renderContent()}
    </div>
  );
};
export default memo(ProductList);
