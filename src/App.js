import logo from "./logo.svg";
import "./App.css";
import ShoesStore from "./Baitap_ShoesStore_Hook/ShoesStore";

function App() {
  return (
    <div className="App">
      <ShoesStore />
    </div>
  );
}

export default App;
